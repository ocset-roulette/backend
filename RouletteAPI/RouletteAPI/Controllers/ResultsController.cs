﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using RouletteAPI.Models;
using RouletteAPI.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RouletteAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ResultsController : ControllerBase
    {
        private readonly IResultRepository _resultRepository;
        public ResultsController(IResultRepository resultRepository)
        {
            _resultRepository = resultRepository;
        }

        [HttpGet]
        public async Task<IEnumerable<Result>> GetResults()
        {
            return await _resultRepository.Get();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Result>> GetResults(int id)
        {
            return await _resultRepository.Get(id);
        }

        [HttpPost]
        public async Task<ActionResult<Result>> PostResults([FromBody] Result result)
        {
            var newResult = await _resultRepository.Create(result);
            return CreatedAtAction(nameof(GetResults), new { id = newResult.Id }, newResult);
        }
    }
}
