﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RouletteAPI.Models
{
    public class Result
    {
        public int Id { get; set; }
        public string PlayerName { get; set; }
        public int InitialChips { get; set; }
        public int PlayedGames { get; set; }
        public int BiggestWin { get; set; }
        public int WonChips { get; set; }
        public int LostChips { get; set; }
    }
}
