﻿using RouletteAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RouletteAPI.Repositories
{
    public interface IResultRepository
    {
        Task<IEnumerable<Result>> Get();
        Task<Result> Get(int id);
        Task<Result> Create(Result result);
        Task Update(Result result);
        Task Delete(int id);
    }
}
