﻿using Microsoft.EntityFrameworkCore;
using RouletteAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RouletteAPI.Repositories
{
    public class ResultRepository : IResultRepository
    {
        private readonly ResultContext _context;

        public ResultRepository(ResultContext context)
        {
            _context = context;
        }

        public async Task<Result> Create(Result result)
        {
            _context.Results.Add(result);
            await _context.SaveChangesAsync();

            return result;
        }

        public async Task Delete(int id)
        {
            var resultToDetele = await _context.Results.FindAsync(id);
            _context.Results.Remove(resultToDetele);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Result>> Get()
        {
            return await _context.Results.ToListAsync();
        }

        public async Task<Result> Get(int id)
        {
            return await _context.Results.FindAsync(id);
        }

        public async Task Update(Result result)
        {
            _context.Entry(result).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}
